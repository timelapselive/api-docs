#!/usr/bin/make -f
BUILDDIR := build
OPTS := --validate

all: $(BUILDDIR)/api.html $(BUILDDIR)/get.html

$(BUILDDIR)/%.html: %.raml
	@mkdir -p $(@D)
	raml2html $(OPTS) -i $< -o $@
